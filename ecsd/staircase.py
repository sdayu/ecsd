'''
    This module is staircase generator.
'''


def staircase(_n):
    '''
        Function will return staircase
    '''
    # Write your code here
    result = ''
    for i in range(1, _n+1):
        result += f'{"#"*i: >{_n}}\n'

    return result.rstrip()

# if __name__ == '__main__':
#     n = int(input().strip())

#     result = staircase(n)
#     print(result)
